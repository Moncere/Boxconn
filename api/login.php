<?php

//redirect user if already logged in
if(isset($_COOKIE['bxnName'])){
  header('Location: ./');
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <meta content="Login" property="og:title">
  <meta content="Login" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/components.css" rel="stylesheet" type="text/css">
  <link href="css/bxndash.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Varela:400","Karla:regular,700"]  }});</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/favicon.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-page-wrapper">
    <img src="images/blg-blue.svg" alt="" class="image">
    <div class="login-container w-form">

      <h1 class="login-head">Login to continue</h1>
     
      <form id="login-form" name="wf-form-Sign-up-Form" data-name="Sign up Form" method="post" ms-login="true" class="memberstack-form">
        <div class="field-wrapper">
          <label  for="node" class="signup-label">Email address</label>
          <input type="email" id="login-email" maxlength="256"  required="" ms-field="email" class="signup-field w-input">
        </div>

        <div class="field-wrapper">
          <label  for="password-2" class="signup-label">Password</label>
          <input type="password" id="login-password" class="signup-field w-input" maxlength="256" name="password-2" data-name="Password 2" ms-field="password" id="password-2" required="">
        </div>

        <div class="secondary-action forgot-password">
          <a ms-forgot="true" href="forgot-password.php" class="login-link">Forgot password?</a>
        </div>

        <input type="submit" value="Login Now" id="login-submit" class="login-button w-button">

      </form>

      <div class="error-message w-form-fail" id="login-error">
      </div>

    </div>

    <div class="secondary-action">
      <div>
        Don&#x27;t have an account yet? Click here to 
        <a href="subscription-plan.html">
          <strong class="bold-text">Sign Up</strong>
        </a>
      </div>
    </div>

  </div>

  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js?site=5f1978088defa977a385d8f0" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>  
  
  <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js"></script>
  <!-- Add the Firebase SDK for Analytics -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-analytics.js"></script>

  <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-firestore.js"></script>

  <script src="js/notify.min.js" type="text/javascript"></script>

  <script src="js/bxndash.js" type="text/javascript"></script>
  <script src="js/config.js" type="module"></script>
  <script src="js/auth.js" type="module"></script>
</body>
</html>