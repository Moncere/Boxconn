<?php

//redirect user if not logged in
if(!isset($_COOKIE['bxnName'])){
  header('Location: login.php');
}

?>
<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>Overview | Boxconn</title>
  <meta content="Boxconn" property="og:title">
  <meta content="Boxconn" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

  <!-- CDN CSS -->
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- LOCAL CSS -->
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/components.css" rel="stylesheet" type="text/css">
  <link href="css/bxndash.css" rel="stylesheet" type="text/css">
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <link href="css/pagination.css" rel="stylesheet" type="text/css">
  <link href="css/micromodal.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
  <script type="text/javascript">
    !function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);
  </script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/favicon.png" rel="apple-touch-icon">
  <!-- Chart.js CDN -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>
<body>
  <div class="page-wrapper">

    <div class="top-nav"><a href="#" class="logo-link w-nav-brand"><img src="./images/logo-white.svg" width="92" alt=""></a>
      <div data-hover="1" data-delay="100" class="dropdown w-dropdown">
        <div class="navigation-item profile-nav w-dropdown-toggle"><img src="./images/logo-white.svg" width="73.5" height="32" alt="" class="profile-pic">
          <div style="color: white">Hi, <span ms-data="first-name"><?= $_COOKIE['bxnName'] ?></span>!</div>
        </div>
        <nav class="nav-dropdown-list w-dropdown-list">
          <div class="webflow-diamond"></div>
          <div class="nav-drop-list-padding">
            <a href="#" class="navigation-item dropdown-nav-item w-inline-block">
              <div class="navigation-icon"><img src="./images/ade-avi.jpg" alt="profile" /></div>
              <div>Profile</div>
            </a>
            
            <a href="tel:+2330551103642" class="navigation-item dropdown-nav-item w-inline-block">
              <div>Helpline: 0551103642</div>
            </a>

            <a href="" id="logout" class="navigation-item dropdown-nav-item w-inline-block">
              <div class="navigation-icon"></div>
              <div>Logout</div>
            </a>
          </div>
        </nav>
      </div>
    </div>

    <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">

      <!-- start navigation bar -->
      <div class="navigation-menu w-tab-menu">
        <a data-w-tab="Overview" id="overview-page" class="navigation-item w-inline-block w-tab-link w--current">
          <div class="navigation-icon"><em class="italic-text-4"><img src="./images/overview.svg" alt="overview" /></em></div>
          <div> Overview</div>
        </a>
        <a data-w-tab="NewDelivery" id="new-delivery" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-2"><img src="./images/dash.svg" alt="new" /></em></div>
          <div>New Delivery</div>
        </a>
        <a data-w-tab="Deliveries" id="deliveries" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-3"><img src="./images/order.svg" alt="deliveries" /></em></div>
          <div>Deliveries</div>
        </a>
        <a data-w-tab="InstantOrders" style="display: none" id="instant-orders" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-3"><img src="./images/box-waiting.svg" alt="instant" /></em></div>
          <div>Instant Orders</div>
        </a>
        <!-- <a data-w-tab="Tab 3" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-4"></em></div>
          <div>Progress</div>
        </a>
        <a data-w-tab="Tab 8" class="navigation-item hide-on-mobile w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-5"></em></div>
          <div>Integrations</div>
        </a> -->
      </div>
      <!-- end navigation bar -->


      <!-- start content -->
      <div class="dash-tab-wrapper w-tab-content">

    <!--  <form style="position: absolute; left: 87% " action="">
        <label style=" font-family: Lato, sans-serif; color:#005376 " name="Filter" id="filter" for="orders">Choose a day</label>
          <select name="cars" id="cars">
            <option value="volvo">Today</option>
            <option value="saab">Past week</option>
            <option value="opel">Past month</option>
           <option value="audi">All time</option>
          </select>
  <br><br>
        <input type="submit" value="Submit">
      </form>
      ------>

        <!-- start overview section -->
        <div data-w-tab="Overview" class="dashboard-section w-tab-pane w--tab-active">
          <div class="container">
            <h3 style="display: inline;">
              Welcome back, <span ms-data="first-name"><?= $_COOKIE['bxnName'] ?>!
            </h3>
            <p></p>
            <div class="dash-row">
              <div class="white-box third" style="height: 165px;">
                <div class="box-padding">
                  <div style="display: flex" class=""><img src="images/Box0.svg" width="50" alt="" >
                  <div style="display:inline;margin-left: 5px; margin-top: 5px; white-space:nowrap; color: #1689FC" class="text-block-2">Total Deliveries</div>
                </div>
                  <h3 style="float: right; color: #1689FC" class="large-number"><span ms-data="budget" id="overview-total"></span></h3>
                  
                </div>
              </div>
              <div class="white-box third" style="height: 165px;">
                <div class="box-padding">
                  <div style="display: flex; class="div-block-2-copy"><img src="images/Box1.svg" width="50" alt="" >
                  <div style="display:inline;margin-left: 5px; margin-top: 10px; white-space:nowrap; color: #17B978">Completed Deliveries</div>
                </div>
                  <h3 style="float: right; color: #17B978" class="large-number"><span ms-data="spend" id="overview-completed"></span></h3>
                  
                </div>
              </div>
              <div class="white-box third mobile-full-box" style="height: 165px;">
                <div class="box-padding">
                  <div style="display: flex" class="div-block-2-copy-2"><img src="images/Box2.svg" width="50" alt=""  >
                  <div style="display:inline;margin-left: 5px; margin-top: 5px;white-space:nowrap; color: #084482 " >Last Delivery</div>
                </div>
                  <!---class="image-3"--->
                  <h3 style="float: right; color: #084482" ms-data="launch" class="large-number" id="overview-last"></h3>
                  
                </div>
              </div>

            </div>

            

             <!-- business analytics overview -->
            <div id="overview-graph" style="display: none" class="dash-row">
              <div class="white-box full-width">
                <div class="box-padding">
                  <div class="w-embed w-script"><canvas id="lineChart"></canvas>
                  </div>
                </div>
              </div>
          <div>
              <h3>Branch Reports
              <!--<img style="float: right; cursor: pointer" src="./images/Box-add.svg" alt=""> -->
              </h3>
          </div>
            </div>

            <div id="overview-sheets" style="display: none" class="dash-row">
            <img src="./images/Box0.svg" alt="">              
            </div>
            
          </div>
        </div>
        <!-- end overview section -->


        <!-- start new delivery section -->
        <div data-w-tab="NewDelivery" class="dashboard-section w-tab-pane">
          <div class="container">
            <h3 class="heading-2">New Delivery</h3>
            <div class="project-grid">
              <div class="template-left">
                <div class="white-box full-width">
                  <div class="box-padding" id="tabs">

                    <form id="wf-form-Delivery-Form" name="wf-form-Delivery-Form" data-name="Delivery Form">

                      <!-- start new delivery step one  -->  <!-----class="tabcontent"---->
                      <div class="tabcontent" style="display: block; border: 0px" id="step-one">
                          <label for="pickup">Pick-Up point</label>
                          <input type="text" class="w-input" autofocus="true" maxlength="256" name="pickup" data-name="pickup" placeholder="Enter pickup location" id="pickup" required>

                          <label for="dropoff" class="field-label-2">Drop-Off Point</label>
                          <input type="text" class="w-input" maxlength="256" name="dropoff" data-name="dropoff" placeholder="Enter dropoff location" id="dropoff" required="">

                          <label for="dropoff-2" class="field-label">Carrier Type</label>
                          <select id="carrier-type" name="field" required="" class="w-select">
                            <option value="">Select a preferred carrier</option>
                            <option disabled value="bicycle">Bicycle</option>
                            <option value="motorbike">Motorbike</option>
                            <option disabled  value="delivery-car">Delivery Car</option>
                            <option disabled  value="delivery-van">Delivery Van</option>
                          </select>

                          <label for="dropoff-2" class="field-label">Delivery Type</label>
                          <select id="delivery-type" name="field" required="" class="w-select">
                            <option value="instant">Instant Delivery</option>
                            <option value="scheduled">Scheduled Delivery</option>
                          </select>

                          <label for="pickup-date" style="display: none;" id="pickupDate" class="field-label-2">Pickup on</label>
                          <input type="datetime" style="display: none;" class="w-input" name="pickup-date" placeholder="Enter pickup date" id="pickup-date">

                          <label for="dropoff-date" style="display: none;" id="dropoffDate" class="field-label-2">Deliver on</label>
                          <input type="datetime" style="display: none;" class="w-input" name="dropoff-date" placeholder="Enter dropoff date" id="dropoff-date">

                          <label class="w-checkbox checkbox-field">
                            <input type="checkbox" id="fragile" name="Fragile-item" data-name="Fragile item" class="w-checkbox-input checkbox">
                            <span for="Fragile item" class="w-form-label">Fragile Item</span>
                          </label>
                      </div>
                      <!-- end new delivery step one -->

                      <!-- start new delivery step two -->
                      <div class="tabcontent" style="display: none; border: 0px" id="step-two">
                        
                        <!-- start step two top pane -->
                        <div class="w-form">
                          <div class="w-row">
                            <div class="column-7 w-col w-col-6">
                              <div class="w-row">
                                <div class="w-col w-col-6">
                                  <a  href="" data-id="step-one" id="new-back" class="tablinks">
                                    <img src="images/back.svg" width="44" alt="" class="image-11">
                                  </a>
                                </div>
                              </div>
                            </div>
                            <code>
                              <div class="w-col w-col-6">
                                <div class="w-row">
                                  <div class="w-col w-col-4">
                                    <img src="images/050-food-delivery.svg" width="40px" alt="" class="image-8">
                                  </div>
                                  <div class="w-col w-col-8">
                                    <div class="phase-2-price-block-2">
                                      <h5 id="estimate"></h5>
                                      <medium id="delivery-distance"></medium>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </code>
                          </div>
                        </div>
                        <!-- end step two top pane -->

                        <hr>

                        <div class="w-form">

                            <label for="name">Instructions</label>
                            <textarea id="instructions" placeholder="Delivery Instructions" maxlength="5000" id="field-2" name="field-2" data-name="Field 2" class="w-input"></textarea>

                            <label for="pickupContact">Pick-Up Contact</label>
                            <input id="pickupContact" type="tel" class="w-input" autofocus="true" maxlength="10" name="pickupContact" placeholder="0500000000" required>

                            <label for="dropoffContact">Drop-Off Contact</label>
                            <input id="dropoffContact" type="tel" class="w-input" autofocus="true" maxlength="10" name="dropoffContact" placeholder="0500000000" required>

                            <label for="item-payment">Item fee</label>
                              <select id="itemPayment" name="itemPayment" required class="w-select">
                                <option value='none'>No Item Fee</option>
                                <option value='collect'>Collect Fee for me</option>
                              </select>
                            <div class="w-row" id="item-fee" style="display: none">
                              <div class="column-12 w-col-12">
                                <label>How much is the fee?</label>
                              <input type="number" class="w-input" maxlength="10" name="itemFee" placeholder="200" id="itemFee" required>
                                </div>
                            </div>

                            <label for="payment-method">Payment Method</label>
                              <section>
                                <input type="radio" class="payment-status" name="payment-status" value="before-delivery" required checked> Pay now
                                <br>
                                <input type="radio" class="payment-status" name="payment-status" value="after-delivery" required> Pay on delivery
                              </section>
                              <select  style="display: none; margin-top: 10px"id="payment-method" name="payment-method" class="w-select">
                                <option value="cash">Cash</option>
                                <option value="paystack">Card / Mobile Money</option>
                              </select>
                              <br>
                        </div>

                      </div>
                      <!-- end new delivery step two -->
                    </form>

                    <a href="" class="tablinks submit-button w-button" data-id="step-two">Continue</a>

                  </div>
                </div>
              </div>
              <div class="template-right">
                <div style="height: 600px" id="new-delivery-map" ref="map"></div>
              </div>
              <div class="filter-drawer"></div>
            </div>
          </div>
        </div>
        <!-- end new delivery section -->
        <div data-w-tab="Deliveries" class="dashboard-section w-tab-pane">
          <div class="container">
            
            <h3 class="heading-2">Deliveries <li class="fa fa-spinner fa-pulse" id="loading-deliveries"></li></h3>

            <div class="project-grid">
              <div class="template-left">
               <div class="white-box full-width">
                <div class="box-padding">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">📦 Ongoing Deliveries</th>
                      </tr>
                    </thead>
                    <tbody style="color: #005376" id="pending-deliveries">
                      <!-- deliveries go here -->
                    </tbody>
                  </table>
                  <div id="pending-deliveries-pagination" class="pagination"></div>
                </div>
              </div>
                <div class="white-box full-width">
                  <div class="box-padding">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <p><strong>✅ Completed Deliveries</strong></p>
                        </tr>
                      </thead>

                      <tbody style="color: #005376" id="completed-deliveries">
                          <!-- deliveries go here -->
                      </tbody>
                    </table>
                    <div id="completed-deliveries-pagination" class="pagination"></div>
                    </div>
                  </div>
                  <div class="white-box full-width">
                    <div class="box-padding">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <p><strong>❌ Cancelled Deliveries</strong></p>
                          </tr>
                        </thead>

                        <tbody style="color: #005376" id="cancelled-deliveries">
                            <!-- deliveries go here -->
                        </tbody>
                      </table>
                      <div id="cancelled-deliveries-pagination" class="pagination"></div>
                    </div>
                  </div>
                  </div>
                <div class="template-right">
                
                <!-- start delivery tracking section -->
                <div class="box-padding">
                  <div class="progress-wrapper">
                    <div class="progress-text-row">
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-8">1</div>
                        </div>
                        <div class="mobile-progress-text">Hero Assigned</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-12">2</div>
                        </div>
                        <div class="mobile-progress-text">Hero at Pick-up</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-11">3</div>
                        </div>
                        <div class="mobile-progress-text">Hero Half way</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-10">4</div>
                        </div>
                        <div class="mobile-progress-text">Hero at Drop-off</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-9">5</div>
                        </div>
                        <div class="mobile-progress-text">Delivery Completed</div>
                      </div>
                    </div>
                    <div class="progress-bar-wrap">
                      <progress class="progress is-info" style="width: 100%" id="delivery-progress" value="0" max="100"></progress>
                      <!-- <div class="progress-bar"></div> -->
                    </div>
                  </div>
                </div>
                <!-- end delivery tracking section -->

                <div style="height: 500px;" id="deliveries-map" ref="map"></div>
              </div>
              <div class="filter-drawer"></div>
            </div>

          </div>
        </div>
        <!-- start deliveries section -->

        <!-- end deliveries section -->

        <!-- start instant orders section -->
        <div data-w-tab="InstantOrders" class="dashboard-section w-tab-pane">
          <div class="container">
            <h3 class="heading">Pending Orders from Today | <?= date('d-m-Y')?></h3>
            <div class="dash-row" id="dataContainer"></div>
            <div class="dash-row">
              <center><div class="pagination" id="pagination"></div></center>
            </div>
          </div>
        </div>
        <!-- end instant orders section -->

      </div>
      <!-- end content -->

    </div>

    <!-- start footer -->
    <!-- <div class="top-nav footer-nav">
      <div class="contain">
        <a href="#" class="logo-link w-nav-brand">
          <img src="images/Group-9339.svg" width="85" alt="" class="image-4">
          <div class="copyright">
            Copyright &copy; Boxconn
          </div>
        </a>
      </div>
      <div class="social-row">
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-facebook"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-twitter"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-youtube"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-facebook"></li>
        </a>
      </div>
    </div> -->
    <!-- end footer -->

    <div class="mobile-footer-spacing"></div>

    <!-- start modals -->

    <!-- start cancel modal -->
    <div class="modal micromodal-slide" id="cancel-delivery" aria-hidden="true">
      <div class="modal__overlay" tabindex="-1"  data-dismiss="modal">
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
          <header class="modal__header">
            <h2 class="modal__title" id="cancel-title">
              Cancel Delivery?
            </h2>
            <button class="modal__close" aria-label="Close modal"  data-dismiss="modal"></button>
          </header>
          <main class="modal__content" id="modal-1-content">
            <p id="cancel-message"></p>
            <input type="hidden" id="cancel-id"/>
          </main>
          <footer class="modal__footer">
            <button class="btn btn-sm submit-button w-button" data-dismiss="modal">No, Don't Cancel</button>
            <button class="btn btn-danger" data-dismiss="modal" id="modal-cancel-accept" >Cancel</button>
          </footer>
        </div>
      </div>
    </div>
    <!-- end cancel modal -->

    <!-- start delivery details modal -->
    <div style="padding-top:100px" class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-m">
        <div class="modal-content">
          <div style="background-color: #17B978" class="modal-header">
            <img src="./images/logo-white.svg" alt="boxconn-white logo" style="width: 100px; float: left" />
            <h4 style="color:white; font-size: 13px" class="modal-title">Delivery information </h4>
          </div>
          <div style="color: #005376; font-family: arial; font-size: 14px " class="modal-body container">

            <div class="row">
              <div class="col-md-3">
                Pickup-point:
              </div>
              <div class="col-md-9" id="delivery-pickup">
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-3">
                Dropoff-point:
              </div>
              <div class="col-md-9" id="delivery-dropoff">
              </div>
            </div>

            <br>

            <div class="row">
              <div class="col-md-3">
                Request Time:
              </div>
              <div class="col-md-9" id="delivery-time">
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                Item Fee:
              </div>
              <div class="col-md-9" id="delivery-itemfee">
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                Instructions:
              </div>
              <div class="col-md-9" id="delivery-instructions">
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                Delivery Fee:
              </div>
              <div class="col-md-9" id="delivery-price">
              </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                Payment:
              </div>
              <div class="col-md-9" id="delivery-payment">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <span id='modal-cancel-button'></span>
            <button type="button" class="btn btn-sm submit-button w-button" data-dismiss="modal">Dismiss</button>
          </div>
        </div>
      </div>
    </div>
    <!-- end delivery details modal -->

    <!-- end modals -->

  </div>

  <div class="ms-iframe"></div>

  <!-- bootstrap -->
  

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>


  <!-- date time picker -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

  <!-- axios -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>

  <!-- moment js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
  
  <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js"></script>
  <!-- Add the Firebase SDK for Analytics -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-analytics.js"></script>

  <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-firestore.js"></script>

  <!-- alert notification -->
  <script src="js/notify.min.js" type="text/javascript"></script>

  <!-- modal -->
  <script type="text/javascript" src="https://unpkg.com/micromodal/dist/micromodal.min.js"></script>

  <!-- paystack -->
  <script type="text/javascript" src="https://js.paystack.co/v1/inline.js"></script>

  <!--  Load Google Places API  -->
  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYfjK7jqijDILHgumoVLZJ5oglAzLUq5E&libraries=places"></script>
  <script src="js/pagination.min.js" type="text/javascript"></script>
  <script src="js/config.js" type="module"></script>
  <script src="js/templates.js" type="module"></script>
  <script src="js/new-delivery.js" type="module"></script>
  <script src="js/process.js" type="module"></script>
  <script src="js/pricing.js" type="module"></script>
  <script src="js/bxndash.js" type="text/javascript"></script>

  <script type="text/javascript">

    var t = new Date(1970, 0, 1); // Epoch

    const orderDetails = (id, data)=>{

      let delivery = JSON.parse(unescape(data))
      t.setSeconds(delivery.requestTime.seconds);

      $('#delivery-pickup').html(delivery.pickup.name)
      $('#delivery-dropoff').html(delivery.dropoff.name)
      $('#delivery-instructions').html(delivery.instructions)
      $('#delivery-itemfee').html(delivery.itemFee)
      $('#delivery-time').html(t.toString().substr(0, 21))
      $('#delivery-price').html(delivery.payment.price)
      delivery.payment.method === 'paystack' ?
      $('#delivery-payment').html('Pay with Momo/Card ' + delivery.payment.status.replace('-', ' ')) :
      $('#delivery-payment').html('Pay with Cash')

      let label = `Delivery from ${delivery.pickup.name} to ${delivery.dropoff.name}`



      // set progress
      if(delivery.status === 'pending'){
        $('#delivery-progress').val(0)
        $('#modal-cancel-button').html(`
          <button type="button" data-dismiss="modal" class="btn btn-danger" data-toggle="modal" data-target="#cancel-delivery" onclick="cancelDelivery('${id}', '${label}')">Cancel</button>
        `)
      }else if(delivery.status === 'assigned'){
        $('#delivery-progress').val(10)
        $('#modal-cancel-button').css('display', 'flex')
      }else if(delivery.status === 'pickup'){
        $('#delivery-progress').val(30)
        $('#modal-cancel-button').css('display', 'flex')
      }else if(delivery.status === 'halfway'){
        $('#delivery-progress').val(50)
        $('#modal-cancel-button').css('display', 'flex')
      }else if(delivery.status === 'dropoff'){
        $('#delivery-progress').val(70)
        $('#modal-cancel-button').css('display', 'flex')
      }else if(delivery.status === 'completed'){
        $('#delivery-progress').val(100)
        $('#modal-cancel-button').css('display', 'none')
      }else{
        $('#delivery-progress').val(0)
        $('#modal-cancel-button').css('display', 'none')
      }

    }

    const cancelDelivery = (id, message)=>{
      $('#cancel-message').html(unescape(message))
      $('#cancel-id').val(id);
    }

    const print = (order)=>{
      let doc = JSON.parse(unescape(order))
      t.setSeconds(doc.requestTime.seconds);
      
      firebase.firestore().collection("deliveries").doc(doc._id).update("print", true)
      let printWindow = window.open('', 'PRINT', 'width=400');
      printWindow.document.write(`<html><head><title>${doc.instructions}</title>`);
      printWindow.document.write('</head><body style="width: 272px">');
      printWindow.document.write(`<div>
      	<div class="third" id="modal-titles">
      		<p><strong>Pick-up Point</strong>: <br>${doc.pickup.name}</p>
      		<p><strong>Drop-off Point</strong>: <br>${doc.dropoff.name}</p>
      		<p><strong>Instructions</strong>: <br><b>${doc.instructions}</b></p>
      		<p><strong>Item Fee</strong>: <br>GHS ${doc.itemFee}</p>
      		<p><strong>Order Time</strong>: <br>${t.toString().substr(0, 21)}</p>
      		<p><strong>Delivery Cost</strong>: <br>GHS ${doc.payment.price}</p>
      		<p><strong>Payment</strong>: <br>${doc.payment.method} [${doc.payment.status}]</p>
      		<hr>
      		<p style="font-size:13px"> Powered by Boxconn Technologies </p>
      	</div>
      	</div>
      </div>`);
      printWindow.document.write('</body></html>');
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
    }

  </script>

</body>
</html>