<?php

//redirect user if not logged in
if(!isset($_COOKIE['bxnName'])){
  header('Location: /login.php');
}

?>
<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>Overview | Boxconn</title>
  <meta content="Admin" property="og:title">
  <meta content="Admin" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/components.css" rel="stylesheet" type="text/css">
  <link href="css/bxndash.css" rel="stylesheet" type="text/css">
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <link href="css/pagination.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: { families: ["Varela:400","Karla:regular,700"]  }});</script>
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/favicon.png" rel="apple-touch-icon">
  <!-- Chart.js CDN -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>
<body>
  <div class="page-wrapper">

    <div class="top-nav"><a href="#" class="logo-link w-nav-brand"><img src="images/Group-9339.svg" width="92" alt=""></a>
      <div data-hover="1" data-delay="100" class="dropdown w-dropdown">
        <div class="navigation-item profile-nav w-dropdown-toggle"><img src="images/Group-9339.svg" width="73.5" height="32" alt="" class="profile-pic">
          <div style="color: white">Hi, <span ms-data="first-name"><?= $_COOKIE['bxnName'] ?></span>!</div>
        </div>
        <nav class="nav-dropdown-list w-dropdown-list">
          <div class="webflow-diamond"></div>
          <div class="nav-drop-list-padding">
            <a href="#" class="navigation-item dropdown-nav-item w-inline-block">
              <div class="navigation-icon"></div>
              <div>Profile</div>
            </a>
            <a href="" id="logout" class="navigation-item dropdown-nav-item w-inline-block">
              <div class="navigation-icon"></div>
              <div>Logout</div>
            </a>
            <a href="tel:+2330551103642" class="navigation-item dropdown-nav-item w-inline-block">
              <div>Helpline: 0551103642</div>
            </a>
          </div>
        </nav>
      </div>
    </div>

    <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">

      <!-- start navigation bar -->
      <div class="navigation-menu w-tab-menu">
        <a data-w-tab="Overview" id="overview-page" class="navigation-item w-inline-block w-tab-link w--current">
          <div class="navigation-icon"><em class="italic-text-4"></em></div>
          <div>Overview</div>
        </a>
        <a data-w-tab="NewDelivery" id="new-delivery" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-2"></em></div>
          <div>New Delivery</div>
        </a>
        <a data-w-tab="Deliveries" id="deliveries" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-3"></em></div>
          <div>Deliveries</div>
        </a>
        <a data-w-tab="InstantOrders" id="instant-orders" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-3"></em></div>
          <div>Instant Orders</div>
        </a>
        <!-- <a data-w-tab="Tab 3" class="navigation-item w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-4"></em></div>
          <div>Progress</div>
        </a>
        <a data-w-tab="Tab 8" class="navigation-item hide-on-mobile w-inline-block w-tab-link">
          <div class="navigation-icon"><em class="italic-text-5"></em></div>
          <div>Integrations</div>
        </a> -->
      </div>
      <!-- end navigation bar -->


      <!-- start content -->
      <div class="dash-tab-wrapper w-tab-content">

        <!-- start overview section -->
        <div data-w-tab="Overview" class="dashboard-section w-tab-pane w--tab-active">
          <div class="container">
            <h3 style="display: inline;">
              Welcome back, <span ms-data="first-name"><?= $_COOKIE['bxnName'] ?>!
            </h3>
            <div class="dash-row">
              <div class="white-box third">
                <div class="box-padding">
                  <div class="div-block-2"><img src="images/046-order.svg" width="23" alt="" class="image-3"></div>
                  <h3 class="large-number"><span ms-data="budget" id="overview-total"></span></h3>
                  <div class="text-block-3">Total Deliveries</div>
                </div>
              </div>
              <div class="white-box third">
                <div class="box-padding">
                  <div class="div-block-2-copy"><img src="images/025-export.svg" width="23" alt="" class="image-3"></div>
                  <h3 class="large-number"><span ms-data="spend" id="overview-completed"></span></h3>
                  <div>Completed Deliveries</div>
                </div>
              </div>
              <div class="white-box third mobile-full-box">
                <div class="box-padding">
                  <div class="div-block-2-copy-2"><img src="images/028-express-delivery.svg" width="23" alt="" class="image-3"></div>
                  <h3 ms-data="launch" class="large-number" id="overview-last"></h3>
                  <div>Last Delivery</div>
                </div>
              </div>

            </div>

             <!-- business analytics overview -->
            <div id="overview-graph" style="display: none" class="dash-row">
              <div class="white-box full-width">
                <div class="box-padding">
                  <div class="w-embed w-script"><canvas id="lineChart"></canvas>
                  </div>
                </div>
              </div>

              <h3>Reports</h3>
            </div>

            <div id="overview-sheets" style="display: none" class="dash-row">              
            </div>

          </div>
        </div>
        <!-- end overview section -->


        <!-- start new delivery section -->
        <div data-w-tab="NewDelivery" class="dashboard-section w-tab-pane">
          <div class="container">
            <h3 class="heading-2">New Delivery</h3>
            <div class="project-grid">
              <div class="template-left">
                <div class="white-box full-width">
                  <div class="box-padding">
                    <div class="w-form">
                      <form id="wf-form-Delivery-Form" name="wf-form-Delivery-Form" data-name="Delivery Form">
                        <label for="pickup">Pick-Up point</label>
                        <input type="text" class="w-input" autofocus="true" maxlength="256" name="pickup" data-name="pickup" placeholder="Enter pickup location" id="pickup" required>

                        <label for="dropoff" class="field-label-2">Drop-Off Point</label>
                        <input type="text" class="w-input" maxlength="256" name="dropoff" data-name="dropoff" placeholder="Enter dropoff location" id="dropoff" required="">

                        <label for="dropoff-2" class="field-label">Carrier Type</label>
                        <select id="carrier-type" name="field" required="" class="w-select">
                          <option value="">Select a preferred carrier</option>
                          <option disabled value="bicycle">Bicycle</option>
                          <option value="motorbike">Motorbike</option>
                          <option disabled  value="delivery-car">Delivery Car</option>
                          <option disabled  value="delivery-van">Delivery Van</option>
                        </select>

                        <label for="dropoff-2" class="field-label">Delivery Type</label>
                        <select id="delivery-type" name="field" required="" class="w-select">
                          <option value="instant">Instant Delivery</option>
                          <option value="scheduled">Scheduled Delivery</option>
                        </select>

                        <label for="pickup-date" style="display: none;" id="pickupDate" class="field-label-2">Pickup on</label>
                        <input type="datetime" style="display: none;" class="w-input" name="pickup-date" placeholder="Enter pickup date" id="pickup-date">

                        <label for="dropoff-date" style="display: none;" id="dropoffDate" class="field-label-2">Deliver on</label>
                        <input type="datetime" style="display: none;" class="w-input" name="dropoff-date" placeholder="Enter dropoff date" id="dropoff-date">

                        <label class="w-checkbox checkbox-field">
                          <input type="checkbox" id="Fragile item" name="Fragile-item" data-name="Fragile item" class="w-checkbox-input checkbox">
                          <span for="Fragile item" class="w-form-label">Fragile Item</span>
                        </label>
                        <input type="submit" value="Continue" class="submit-button w-button">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="template-right">
                <div data-widget-latlng="5.6358281,-0.1613592" data-widget-tooltip="Pickup Point" data-widget-style="terrain" data-widget-zoom="14" map="map" class="map w-widget w-widget-map"></div>
              </div>
              <div class="filter-drawer"></div>
            </div>
          </div>
        </div>
        <!-- end new delivery section -->
        <div data-w-tab="Deliveries" class="dashboard-section w-tab-pane">
          <div class="container">
            
            <h3 class="heading-2">Deliveries <li class="fa fa-spinner fa-pulse" id="loading-deliveries"></li></h3>

            <div class="project-grid">
              <div class="template-left">
               <div class="white-box full-width">
                <div class="box-padding">
                  <p><strong>📦 Ongoing Deliveries</strong></p>
                  <hr>
                  <div id="pending-deliveries"></div>
                  <div id="pending-deliveries-pagination" class="pagination"></div>
                </div>
              </div>
                <div class="white-box full-width">
                <div class="box-padding">
                  <p><strong>✅ Completed Deliveries</strong></p>
                  <hr>
                  <div id="completed-deliveries"></div>
                  <div id="completed-deliveries-pagination" class="pagination"></div>
                </div>
              </div>
              <div class="white-box full-width">
                <div class="box-padding">
                  <p><strong>❌ Cancelled Deliveries</strong></p>
                  <hr>
                  <div id="cancelled-deliveries"></div>
                  <div id="cancelled-deliveries-pagination" class="pagination"></div>
                </div>
              </div>
              </div>
              <div class="template-right">
                
                <!-- start delivery tracking section -->
                <div class="box-padding">
                  <div class="progress-wrapper">
                    <div class="progress-text-row">
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-8">1</div>
                        </div>
                        <div class="mobile-progress-text">Hero Assigned</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-12">2</div>
                        </div>
                        <div class="mobile-progress-text">Hero at Pick-up</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-11">3</div>
                        </div>
                        <div class="mobile-progress-text">Hero Half way</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-10">4</div>
                        </div>
                        <div class="mobile-progress-text">Hero at Drop-off</div>
                      </div>
                      <div class="progress-text-column">
                        <div class="progress-icon">
                          <div class="text-block-9">5</div>
                        </div>
                        <div class="mobile-progress-text">Delivery Completed</div>
                      </div>
                    </div>
                    <div class="progress-bar-wrap">
                      <progress class="progress is-info" :value="progress" max="100">15%</progress>
                      <!-- <div class="progress-bar"></div> -->
                    </div>
                  </div>
                </div>
                <!-- end delivery tracking section -->

                <div style="height: 500px" id="deliveries-map" ref="map"></div>
              </div>
              <div class="filter-drawer"></div>
            </div>

          </div>
        </div>
        <!-- start deliveries section -->

        <!-- end deliveries section -->

        <!-- start instant orders section -->
        <div data-w-tab="InstantOrders" class="dashboard-section w-tab-pane">
          <div class="container">
            <h3 class="heading">Pending Orders from Today | <?= date('d-m-Y')?></h3>
            <div class="dash-row" id="dataContainer"></div>
            <div class="dash-row">
              <center><div class="pagination" id="pagination"></div></center>
            </div>
          </div>
        </div>
        <!-- end instant orders section -->

      </div>
      <!-- end content -->

    </div>

    <!-- start footer -->
    <div class="top-nav footer-nav">
      <div class="contain">
        <a href="#" class="logo-link w-nav-brand">
          <img src="images/Group-9339.svg" width="85" alt="" class="image-4">
          <div class="copyright">
            Copyright &copy; <?= date('Y')?> Boxconn
          </div>
        </a>
      </div>
      <div class="social-row">
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-facebook"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-twitter"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-youtube"></li>
        </a>
        <a href="#" class="social-link w-inline-block">
          <li class="fa fa-facebook"></li>
        </a>
      </div>
    </div>
    <!-- end footer -->

    <div class="mobile-footer-spacing"></div>

  </div>
  <div class="ms-iframe"></div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js?site=5f1978088defa977a385d8f0" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

  <!-- date time picker -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


  <!-- moment js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
  
  <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-app.js"></script>
  <!-- Add the Firebase SDK for Analytics -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-analytics.js"></script>

  <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.3.0/firebase-firestore.js"></script>


  <script src="js/notify.min.js" type="text/javascript"></script>
  <script src="js/pagination.min.js" type="text/javascript"></script>
  <script src="js/config.js" type="module"></script>
  <script src="js/templates.js" type="module"></script>
  <script src="js/process.js" type="module"></script>
  <script src="js/new-delivery.js" type="module"></script>
  <script src="js/bxndash.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <!--  Load Google Places API  -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYfjK7jqijDILHgumoVLZJ5oglAzLUq5E&libraries=places&region=GH"></script>

  <!--  Set autocomplete input  -->
  <script>
  let gpaInput = document.getElementById("pickup");
  autocomplete = new google.maps.places.Autocomplete(gpaInput);
  let place = autocomplete.gpaInput.getPlace();
	var lat = place.geometry.location.lat('map');
	var lng = place.geometry.location.lat('map');
</script>
  <script>
  let gpaInput2 = document.getElementById("dropoff");
  autocomplete = new google.maps.places.Autocomplete(gpaInput2);
</script>
</body>
</html>