const calculate = async (db, data) => {
    const { distance, duration, type } = data;

    let doc = await db.collection("pricing").doc(type).get()

    let pricing = doc.data()
        
    let costPerMin = pricing.per_minute * duration
    let costPerKm = pricing.per_km * distance

    let instant_price = pricing.base_fare + costPerKm + costPerMin;
                        
    if (instant_price < pricing.minimum) {
        instant_price = pricing.minimum;
    }

    if(instant_price == NaN){
        instant_price = pricing.minimum;
    }

    return instant_price.toFixed(2)
}

const business = async (db, data) => {
    const { distance, duration, type } = data;

    let doc = await db.collection("pricing").doc(type).get()

    let pricing = doc.data()
        
    let costPerMin = pricing.per_minute * duration
    let costPerKm = pricing.per_km * distance

    let instant_price = 10;

    if(distance > 5){

        instant_price = pricing.base_fare + costPerKm + costPerMin
                        
        if (instant_price < 10) {
            instant_price = 10;
        }

        if(instant_price == NaN){
            instant_price = pricing.minimum;
        }
    }

    return instant_price.toFixed(2)
}

export { calculate, business }
