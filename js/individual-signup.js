import { auth, db } from './config.js'

$('#sign-up').on('submit', async(e) => {
    e.preventDefault();

    $('#signup-btn').val('Please wait...')

    const data = {
        personName: $('#signup-fullname').val(),
        createdOn: new Date(),
        emailAddress: $('#signup-emailaddress').val(),
        phoneNumber: $('#signup-phonenumber').val(),
        lastActive: new Date(),
        userType: 'individual'
    }

    //number edit
    if(data.phoneNumber.charAt(0) == '0'){
        data.phoneNumber = data.phoneNumber.replace("0", "+233")
    }else{
        data.phoneNumber = '+233'+data.phoneNumber
    }

    var form = new FormData();
    form.append("Name", data.personName);
    form.append("User Type", 'Individual');
    form.append("Email Address", data.emailAddress);
    form.append("Phone Number", data.phoneNumber);
    form.append("Last Active", new Date().toString().substr(0, 21));

    try {
        await auth.createUserWithEmailAndPassword(data.emailAddress, $('#signup-password').val()).then(user=>{
        auth.currentUser.updateProfile({
            displayName: data.personName,
        }).then(function(u) {
            // Update successful.
            auth.currentUser.sendEmailVerification()
            db.collection("users").doc(auth.currentUser.uid).set(
                data
            ).then(doc=>{

                fetch('https://script.google.com/macros/s/AKfycbw-scT85oK8gpLoV2Xg26KBEzPoKHykM0pTDjJzJCavYFLmuHV9jVGUEg/exec', { method: 'POST', body: form})
                .then(response => {
                    $.notify('Registration successful, you can now sign in.', "success");
                    $('#signup-btn').val('Complete Signup')
                    setInterval(()=>{
                        window.location.href = './'
                    }, 1000)
                })
            .catch(error => {})
            })
        }).catch(function(error) {
            // An error happened.
        });
        })
    } catch (e) {
        $('#signup-btn').val('Complete Signup')
        $.notify(e.message, "error");
    }
})