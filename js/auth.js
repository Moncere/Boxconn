import { auth, db } from './config.js'

//handle login
$('#login-form').on('submit', async e=>{
    e.preventDefault()
    const email = $('#login-email').val()
    const password = $('#login-password').val()
    $('#login-submit').val('Please wait...')
    $('#login-submit').attr('disabled', true)

    auth.signInWithEmailAndPassword(email, password)
    .then(async (userCredential) => {
        // Signed in 
        var user = userCredential.user
        let account = await db.collection("users").doc(user.uid).get()

        if(account){

            if(account.data().userType == 'business'){
                //fecth the business' branches if it has any
                let branches = await db.collection('branches').where('businessId', '==', user.uid).get()

                if(branches.docs.length > 0){

                    let branchIds = []
                    let brancheNames = []
                    let brancheSheets = []
                    let brancheScripts = []
                    let brancheLocations = []
                    for(var branch of branches.docs){
                        branchIds.push(branch.id)
                        brancheNames.push(branch.data().name)
                        brancheSheets.push(branch.data().sheet)
                        brancheScripts.push(branch.data().script)
                        brancheLocations.push(branch.data().location)
                    }

                    document.cookie = "bxnBranchIds="+JSON.stringify(branchIds)
                    document.cookie = "bxnBranchNames="+JSON.stringify(brancheNames)
                    document.cookie = "bxnBranchSheets="+JSON.stringify(brancheSheets)
                    document.cookie = "bxnBranchScripts="+JSON.stringify(brancheScripts)
                    document.cookie = "bxnBranchLocations="+JSON.stringify(brancheLocations)
                    document.cookie = "bxnIsBranch=false"
                }else{
                    let branch = await db.collection('branches').doc(user.uid).get()
                    if(branch.exists){
                        document.cookie = "bxnIsBranch=true"
                    }else{
                        document.cookie = "bxnIsBranch=false"
                    }
                }
            }

            document.cookie = "bxnUid="+user.uid;
            document.cookie = "bxnType="+account.data().userType;
            document.cookie = "bxnName="+user.displayName;
            document.cookie = "bxnEmail="+user.email;
            document.cookie = "bxnImg="+user.photoURL;
            document.cookie = "unlocked=true";
            window.location.href = './'
        }else{
            $('#login-submit').val('Login')
            $('#login-submit').attr('disabled', false)
            $.notify("You are not permitted to view the page you are trying to access.", "error");
        }
    })
    .catch(e=>{
        $('#login-submit').val('Login')
        $('#login-submit').attr('disabled', false)
        $.notify(e.message, "error");
    })
})

//handle reset password
$('#reset-form').on('submit', async e=>{
    e.preventDefault()
    const email = $('#reset-email').val()
    $('#reset-submit').val('Please wait...')
    $('#reset-submit').attr('disabled', true)

    auth.sendPasswordResetEmail(email).then(function() {
        $('#reset-submit').val('Reset Password')
        $('#reset-submit').attr('disabled', false)
        $('#reset-form').trigger('reset')
        $.notify("We have sent you a password reset link, please check you email.", "success");
        setInterval(()=>{
            window.location.href = './'
        }, 1000)
    }).catch(function(error) {
        $('#reset-submit').val('Reset Password')
        $('#reset-submit').attr('disabled', false)
        $.notify("There was an error resetting you password, please try again or contact support.", "error");
    });      

})