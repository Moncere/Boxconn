import { auth, db } from './config.js'
import { calculate, business } from './pricing.js'

let isBranch
const uid = document.cookie.split(';').filter(cookie=>cookie.includes('bxnUid'))[0].split('=')[1]
const email = document.cookie.split(';').filter(cookie=>cookie.includes('bxnEmail'))[0].split('=')[1]
const isBusiness = document.cookie.includes('bxnType=business')
if(isBusiness){
	isBranch = document.cookie.split(';').filter(cookie=>cookie.includes('bxnIsBranch'))[0].split('=')[1]
}
let branchScript = null
let currentStep = 1
let map

//define order object
let order = {
	requestedBy: uid,
    pickup: {
      geo: null,
      contact: null,
      name: null
    },
    dropoff: {
      geo: null,
      contact: null,
      name: null
    },
    carrier: '',
    fragile: false,
    requestTime: firebase.firestore.FieldValue.serverTimestamp(),
    responseTime: null,
    deliveryTime: firebase.firestore.FieldValue.serverTimestamp(),
    deliverOn: firebase.firestore.FieldValue.serverTimestamp(),
    rider: null,
    payment: {
      price: 0,
      status: 'before-delivery',
      method: 'paystack',
      paid: false
    },
    deliveryType: 'instant',
    status: "pending",
    instructions: '',
    itemFee: 0,
}


function marker(point){

	let title, latlng, icon, label

	if(point === 'pickup'){
		title = order.pickup.name
		latlng = new window.google.maps.LatLng(order.pickup.geo.latitude, order.pickup.geo.longitude)
		icon = "https://maps.google.com/mapfiles/ms/icons/green-dot.png"
		label = 'Pickup from'
	}else{
		title = order.dropoff.name
		latlng = new window.google.maps.LatLng(order.dropoff.geo.latitude, order.dropoff.geo.longitude)
		icon = "https://maps.google.com/mapfiles/ms/icons/blue-dot.png"
		label = 'Dropoff at'
	}

	let m = new window.google.maps.Marker({
		position: latlng,
		draggable: false,
		animation: window.google.maps.Animation.DROP,
		title: title,
		icon: {
		  	url: icon
		}
	});

	m.setMap(map);
	let info = new window.google.maps.InfoWindow({
	content: `<p><strong>${label}</strong></p>
	      <p>${title}</p>`
	});
	info.open(map, m);
}

// new delivery steps listener
$('.tablinks').on('click', function(evt) {

  	evt.preventDefault()

  	const step = $(this).data('id')

  	if(step == 'step-two'){

  		//check if already on step two to submit form or move to second screen
  		if(currentStep == 2){

  			if(order.pickup.contact != null && order.dropoff.contact != null){

  				if($('#itemPayment').val() === 'none' || ($('#itemFee').val() > 0 && $('#itemPayment').val() === 'collect')){
	  				if(order.payment.status === 'before-delivery'){
	  					paystack()
	  				}else{
	  					finish()
	  				}
	  			}else{
	  				$.notify("Please provide the item fee to collect or select 'No item fee'", "warn");
	  			}

  			}else{
  				$.notify("Please provide pickup and drop-off contacts", "warn");
  			}


  		}else{
	  		// ensure form has been filled before moving to next step
			if(order.pickup.geo != null && order.dropoff.geo != null && order.carrier != ''){
				if(order.deliveryType == 'scheduled'){

				}else{
					currentStep++
					move(evt, step)
				}
			}else{
				$.notify("Please fill in all field to proceed!", "warn");
			}
		}
	}else{
		currentStep--
		move(evt, step)
		//clear distance and price
		$('#delivery-distance').html('')
		$('#estimate').html('')
	}
})

function paystack(){

	let amount = parseFloat(order.payment.price) + parseInt(order.itemFee)

    let handler = PaystackPop.setup({

      key: 'pk_live_1ff473f4cb875e1471ecea48277c0a05bd265538',
      email: email,
      currency: "GHS",
      amount: amount*100,
      ref: ''+Math.floor((Math.random() * 1000000000) + 1),
      onClose: function(){

      },
      callback: function(response){
        if(response.status == 'success'){
      		finish()
        }else{
      		$.notify(response.message, "error");
        }
      }
    });

    handler.openIframe();
}

async function finish(){

	//submit form input
	db.collection('deliveries').add(order)
		.then((doc)=>{
			//reset form and step on success
			$.notify('We have receied your order, a rider will be assigned soon.', "success");
			$('#wf-form-Delivery-Form').trigger('reset')
			$('#new-back').trigger('click')
			$('#deliveries').trigger('click')

			//send telegram notification
			let telegramData = {
				"account_name": email,
				"pickup_point": encodeURI(`https://www.google.com/maps/search/?api=1&query=${order.pickup.name}`),
				"dropoff_point": encodeURI(`https://www.google.com/maps/search/?api=1&query=${order.dropoff.name}`),
				"dropoff_contact":order.dropoff.contact,
				"pickup_contact": order.pickup.contact,
				"delivery_type" : order.deliveryType,
				"pick up": new Date(order.requestTime),
				"deliver by": new Date(order.deliverOn),
				"price": order.payment.price,
				"order_time": moment().format('LT'),
				"instruction": order.instructions,
				"item_fee": order.itemFee,
            }

			fetch('https://v1.nocodeapi.com/boxconn/telegram/RgJDkrpBRzToZrTj', { method: 'POST', body: data})
                .then(response => {})
                .catch(error => {})


			if(isBranch == 'false'){
				if(branchScript != null){
					var data = new FormData();
		            data.append("BXN Order Number", doc.id);
		            data.append("Food Instructions", order.instructions);
		            data.append("Food Cost", order.itemFee);
		            data.append("Delivery Cost", order.payment.price);
		            data.append("Status", "pending");
		            data.append("PickUp Point", order.pickup.name);
		            data.append("Pickup Contact", order.pickup.contact);
		            data.append("Dropoff Point", order.dropoff.name);
		            data.append("Customer Contact", order.dropoff.contact);
		            data.append("Payment Method", order.payment.method);
		            data.append("BXN Rider Name", "");

		            fetch(branchScript, { method: 'POST', body: data})
		                .then(response => {})
		                .catch(error => {} )
				}
			}
		})
		.catch(e=>{
			// display error if write fails
			console.log(e)
			$.notify('Something went wrong with your order, please refresh and try again', "error");
		})
}

// move to next step or back
function move(evt, step){
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(step).style.display = "block";
	evt.currentTarget.className += " active";
	getPrice()
}

//initialize auto complete
function initMap(){

  const center = { lat: 5.6358281, lng: -0.1613592 };

  map = new google.maps.Map(document.getElementById("new-delivery-map"), {
		center: { lat: 5.6358281, lng: -0.1613592 },
		zoom: 16,
	});


  // Create a bounding box with sides ~10km away from the center point
  const defaultBounds = {
    north: center.lat + 0.1,
    south: center.lat - 0.1,
    east: center.lng + 0.1,
    west: center.lng - 0.1,
  };

  const pickup = document.getElementById("pickup");
  const dropoff = document.getElementById("dropoff");

  const options = {
    bounds: defaultBounds,
    componentRestrictions: { country: "gh" },
    fields: ["address_components", "formatted_address", "vicinity", "geometry", "name"],
    origin: center,
    strictBounds: false,
    types: ["establishment"],
  };


  //setup autocomplete fields
  const autocomplete1 = new google.maps.places.Autocomplete(pickup, options);
  const autocomplete2 = new google.maps.places.Autocomplete(dropoff, options);

  autocomplete1.addListener("place_changed", () => {
    const place = autocomplete1.getPlace();

    if(place.geometry){
      	order.pickup.name = place.name +', '+place.vicinity
      	order.pickup.geo = {
	        latitude: place.geometry.location.lat(),
	        longitude: place.geometry.location.lng()
      	}

      	marker('pickup')

		if(isBusiness){

			if(isBranch == 'false'){
				try{
					let branches = JSON.parse(document.cookie.split(';').filter(cookie=>cookie.includes('bxnBranchIds'))[0].split('=')[1])
					let locations = JSON.parse(document.cookie.split(';').filter(cookie=>cookie.includes('bxnBranchLocations'))[0].split('=')[1])
					let scripts = JSON.parse(document.cookie.split(';').filter(cookie=>cookie.includes('bxnBranchScripts'))[0].split('=')[1])

					locations.map((branch, index) => {
						if(branch == order.pickup.name){
							order.requestedBy = branches[index]
							branchScript = scripts[index]
						}
					})
				}catch(e){
				}
			}
		}

    }else{
      $.notify("Please select a known landmark for pickup...", "warn");
    }

  })

  autocomplete2.addListener("place_changed", () => {
    const place = autocomplete2.getPlace();

    if(place.geometry){
      	order.dropoff.name = place.name +', '+place.vicinity
      	order.dropoff.geo = {
	        latitude: place.geometry.location.lat(),
	        longitude: place.geometry.location.lng()
		}

      	marker('dropoff')
    }else{
      $.notify("Please select a known landmark for dropoff...", "warn");
    }

  })
}

//listen for pickup contact change
$('#pickupContact').on('change', (e)=>{
	//set pickup contact on order object
	order.pickup.contact = e.target.value
});

//listen for dropoff contact change
$('#dropoffContact').on('change', (e)=>{
	//set dropoff contact on order object
	order.dropoff.contact = e.target.value
});

//listen for item fee change
$('#itemFee').on('input', (e)=>{
	//set item fee on order object
	if(e.target.value != ''){
		order.itemFee = parseInt(e.target.value)
	}else{
		order.itemFee = 0
	}
});

//listen for instructions input
$('#instructions').on('input', (e)=>{
	//set item fee on order object
	order.instructions = e.target.value
});

//listen for payment method change
$('#payment-method').on('change', (e)=>{
	//set payment method on order object
	order.payment.method = e.target.value
});

//listen for payment status change
$('.payment-status').on('change', (e)=>{
	//set payment status on order object
	order.payment.status = e.target.value
	if(e.target.value == 'after-delivery'){
		order.payment.method = 'cash'
		$('#payment-method').css('display', 'flex')
	}else{
		order.payment.method = 'paystack'
		$('#payment-method').css('display', 'none')
	}
});

//listen for carrier type change
$('#carrier-type').on('change', (e)=>{
	//set delivery type on order object
	order.carrier = e.target.value
});

//listen for fragile change
$('#fragile').on('change', (e)=>{
	//set delivery type on order object
	order.fragile = e.target.checked
});

//listen for collect fee change
$('#itemPayment').on('change', (e)=>{

	//set collect fee on order object
	$('#itemFee').val(0)
	order.itemFee = 0

	//display field if delivery has item fee
	if(e.target.value == 'none'){
		$('#item-fee').css('display', 'none')
	}else{
		$('#item-fee').css('display', 'flex')
	}
	
});

//listen for delivery type change
$('#delivery-type').on('change', (e)=>{

	//set delivery type on order object
	order.deliveryType = e.target.value

	//show date fields if delivery is scheduled
	if(e.target.value == 'scheduled'){
		$('#pickup-date').css('display', 'flex')
		$('#dropoff-date').css('display', 'flex')
		$('#pickup-date').flatpickr({
			enableTime: true,
    		dateFormat: "Y-m-d H:i"
		});
		$('#dropoff-date').flatpickr({
			enableTime: true,
    		dateFormat: "Y-m-d H:i"
		});
		$('#pickupDate').css('display', 'flex')
		$('#dropoffDate').css('display', 'flex')
	}else{
		$('#pickup-date').css('display', 'none')
		$('#dropoff-date').css('display', 'none')
		$('#pickupDate').css('display', 'none')
		$('#dropoffDate').css('display', 'none')
	}
})

function getPrice(){

	let pick = order.pickup.geo
	let drop = order.dropoff.geo
	let directionsService = new google.maps.DirectionsService();

    directionsService.route(
    {
      origin: {
        lat: pick.latitude,
        lng: pick.longitude
      },
      destination: {
        lat: drop.latitude,
        lng: drop.longitude
      },
      travelMode: google.maps.TravelMode.DRIVING,
      // waypoints: stops
    },
    async (response, status) => {
		if (status === "OK") {

			let distance = response.routes[0].legs[0].distance.value/1000
			let duration = response.routes[0].legs[0].duration.value/60
			
			if(email === 'callcenter@mascofoods.com'){
		  		order.payment.price = await business(db, { distance, duration, type: order.carrier })
			}else{
		  		order.payment.price = await calculate(db, { distance, duration, type: order.carrier })
			}

			//sanitize and ensure price is in number format not string
			order.payment.price = parseFloat(order.payment.price).toFixed(2)

			//display distance and price
			$('#delivery-distance').html(response.routes[0].legs[0].distance.text)
			$('#estimate').html('GH₵ '+ order.payment.price )
		} else {
			$.notify("There was an error fetching the delivery estimate, refresh page and try again.", "warn");
		}
	})
}

initMap()