import { auth, db } from './config.js'
import { deliveryTemplate, orderTemplate } from './templates.js'

//declare variables that are constant throughout
const cookies = document.cookie
const uid = cookies.split(';').filter(cookie=>cookie.includes('bxnUid'))[0].split('=')[1]
const isBusiness = cookies.includes('bxnType=business')
const start = moment().startOf('day').toDate()
const end = moment().endOf('day').toDate()
let page = 0
let map

window.addEventListener('load', function() {
	function updateOnlineStatus(event) {
		var condition = navigator.onLine ? "online" : "offline";
		console.log(condition)
	}

	window.addEventListener('online',  updateOnlineStatus);
	window.addEventListener('offline', updateOnlineStatus);
})

$(document).ready(function() {
  	if(isBusiness){
  		$('#instant-orders').css('display', 'flex')
		let isBranch = cookies.split(';').filter(cookie=>cookie.includes('bxnIsBranch'))[0].split('=')[1]
		isBranch === 'true' ? $('#new-delivery').css('display', 'none') : $('#new-delivery').css('display', 'flex')
  	}else{
		$('#instant-orders').css('display', 'none')
	}

	loadOverview()

});

$('#overview-page').on('click', async()=>{
	loadOverview()
	page = 0
})

$('#new-delivery').on('click', async()=>{
	document.title = 'New Delivery | Boxconn'
	$('#wf-form-Delivery-Form').trigger('reset')
	page = 1
})

$('#deliveries').on('click', async()=>{
	document.title = 'Deliveries | Boxconn'
	map = new google.maps.Map(document.getElementById("deliveries-map"), {
		center: { lat: 5.6358281, lng: -0.1613592 },
		zoom: 16,
	});

	loadDeliveries()
	page = 2
})

$('#instant-orders').on('click', async()=>{
	document.title = 'Instant Orders | Boxconn'
	loadOrders()
	page = 3
})

const loadOverview = async ()=>{

	document.title = 'Overview | Boxconn'
	$('#overview-loading').css('display', 'flex')
	$.notify("Refreshing overview...", "info");

	//get the logged user type
	let total = 0, completed = 0, last = ''
	
	if(isBusiness){

		let isBranch = cookies.split(';').filter(cookie=>cookie.includes('bxnIsBranch'))[0].split('=')[1]
		let branches = []
		const totals = []
		const names = ['Head Quaters']
		const completes = []
		const cancelled = []
		let total = 0
		let completed = 0
		let last = '' 

		//fecth the business' branches if it has any
		if(isBranch == 'false'){
			try{
				branches = JSON.parse(cookies.split(';').filter(cookie=>cookie.includes('bxnBranchIds'))[0].split('=')[1])
			}catch(e){
				branches = []
			}
		}

		//load account overview
		const results = await db.collection('deliveries')
				.where('requestedBy', '==', uid)
				.where('requestTime', '<', end).where('requestTime', '>', start)
				.get()

		total = results.docs.length
		totals.push(total)
		completed = results.docs.filter(doc=>doc.data().status == 'completed').length
		completes.push(completed)
		cancelled.push(results.docs.filter(doc=>doc.data().status == 'cancelled').length)

		try{
			last = results.docs.filter(doc=>doc.data().status == 'completed')[completed-1].data().deliveryTime.toDate().toString().substr(11, 21)
		}catch(e){
			last = ''
		}

        $('#overview-total').html(total)
        $('#overview-completed').html(completed)
        $('#overview-last').html(last)
		$('#overview-loading').css('display', 'none')

		// if account has branches add branches overview
		if(branches.length > 0){

			let nm = JSON.parse(cookies.split(';').filter(cookie=>cookie.includes('bxnBranchNames'))[0].split('=')[1])
			for(var name of nm){
				names.push(name)
			}

			const sheets = JSON.parse(cookies.split(';').filter(cookie=>cookie.includes('bxnBranchSheets'))[0].split(/=(.+)/)[1])

			for(var branch of branches){

				const results = await db.collection('deliveries')
					.where('requestedBy', '==', branch)
					.where('requestTime', '<', end).where('requestTime', '>', start)
					.get()

				//make an array of the deliveries from branches for graph
				totals.push(results.docs.length)
				completes.push(results.docs.filter(doc=>doc.data().status == 'completed').length)
				cancelled.push(results.docs.filter(doc=>doc.data().status == 'cancelled').length)

				//append summary counts
				total += results.docs.length
				completed += results.docs.filter(doc=>doc.data().status == 'completed').length

				//update overview
				$('#overview-total').html(total)
		        $('#overview-completed').html(completed)
		        $('#overview-sheets').html('')
		        $('#overview-graph').css('display', 'flex')
		        $('#overview-sheets').css('display', 'flex')
			}

			try{

				var ctx = document.getElementById('lineChart').getContext('2d');
				var chart = new Chart(ctx, {
					// The type of chart we want to create
					type: 'line',
					// The data for our dataset
					data: {
					  	labels: names,
					  	datasets: [{
					      	label: 'Total Deliveries',
					      	borderDash: [10],
					      	borderColor: 'rgb(0, 83, 118, 1)',
					      	data: totals,
					      	hoverBackgroundColor: 'rgb(0, 83, 118, 1)'
					  },{
				      		label: 'Completed Deliveries',
				      		backgroundColor: 'rgb(217, 246, 238, 0.5)',
				      		borderColor: 'rgb(2, 195, 142, 1)',
				      		data: completes
					  },{
				      		label: 'Cancelled Deliveries',
				      		backgroundColor: 'rgb(246, 174, 159, 1)',
				      		borderColor: 'rgb(234, 0, 27, 0.5)',
				      		data: cancelled
					  }]
					},
					// Configuration options go here
					options: {}
				});
				}catch(e){}

				for(var x = 1; x < sheets.length; x++){
					$('#overview-sheets').append(`
						<div class="white-box third">
					        <div class="box-padding">
					          <a target="_blank" href='${sheets[x-1]}'>${names[x]}</a>
					        </div>
					  	</div>`)
				}

		}

		$('#overview-loading').css('display', 'none')
        $.notify("Overview up to date!", "success");

	}else{
		const results = await db.collection('deliveries')
			.where('requestedBy', '==', uid)
			.where('requestTime', '<', end).where('requestTime', '>', start)
			.get()

		let total = results.docs.length
		let completed = results.docs.filter(doc=>doc.data().status == 'completed').length
		let last = '' 
		try{
			last = results.docs.filter(doc=>doc.data().status == 'completed')[completed-1].data().deliveryTime.toDate().toString().substr(0, 10)
		}catch(e){
			last = ''
		}

        $('#overview-total').html(total)
        $('#overview-completed').html(completed)
        $('#overview-last').html(last)
		$('#overview-loading').css('display', 'none')
    //    $.notify("Overview up to date!", "success");

	}
}


//method to fetch deliveries
const loadDeliveries = async ()=>{
	
	let isBranch

	if(isBusiness){
		isBranch = cookies.split(';').filter(cookie=>cookie.includes('bxnIsBranch'))[0].split('=')[1]
	}
	let branches = []
	let pending = []
	let completed = []
	let cancelled = []

//	$.notify("Fetching your deliveries...", "info");

	//fecth the business' branches if it has any
	if(isBranch === 'false'){
		try{
			branches = JSON.parse(cookies.split(';').filter(cookie=>cookie.includes('bxnBranchIds'))[0].split('=')[1])
		}catch(e){
			branches = []
		}
	}

	if(branches.length > 0){

		//load account deliveries
		let results = await db.collection('deliveries')
				.where('requestedBy', '==', uid)
				// .where('status', 'in', ['pickup', 'pending', 'assigned', 'halfway', 'dropoff'])
				.where('requestTime', '<', end).where('requestTime', '>', start)
				.orderBy("requestTime", "desc")
				.get()

		for(var doc of results.docs){
			if(doc.data().status == 'completed'){
				completed.push(doc)
			}else if(doc.data().status == 'cancelled'){
				cancelled.push(doc)
			}else{
				pending.push(doc)
			}
		}

		//display deliveries after fetching
		renderDeliveries(pending, 'pending')
		renderDeliveries(completed, 'completed')
		renderDeliveries(cancelled, 'cancelled')

		//if account has branches fetch branches deliveries
		for(var branch of branches){

			let results = await db.collection('deliveries')
				.where('requestedBy', '==', branch)
				// .where('status', 'in', ['pickup', 'pending', 'assigned', 'halfway', 'dropoff'])
				.where('requestTime', '<', end).where('requestTime', '>', start)
				.orderBy("requestTime", "desc")
				.get()

			for(var doc of results.docs){
				if(doc.data().status == 'completed'){
					completed.push(doc)
				}else if(doc.data().status == 'cancelled'){
					cancelled.push(doc)
				}else{
					pending.push(doc)
				}
			}

			//display deliveries after fetching
			renderDeliveries(pending, 'pending')
			renderDeliveries(completed, 'completed')
			renderDeliveries(cancelled, 'cancelled')
		}

		$('#loading-deliveries').css('display', 'none')
	//	$.notify("Deliveries up to date...", "success");

	}else{

		const orders = []

		db.collection('deliveries')
			.where('requestedBy', '==', uid)
			.where('requestTime', '<', end).where('requestTime', '>', start)
			.orderBy("requestTime", "desc")
			.get()
			.then(snapshot=>{
				snapshot.forEach(doc=>{
					if(doc.data().status == 'completed'){
						completed.push(doc)
					}else if(doc.data().status == 'cancelled'){
						cancelled.push(doc)
					}else{
						pending.push(doc)
					}
				})
				//display deliveries after fetching
				renderDeliveries(pending, 'pending')
				renderDeliveries(completed, 'completed')
				renderDeliveries(cancelled, 'cancelled')
				$('#loading-deliveries').css('display', 'none')
		//		$.notify("Deliveries up to date...", "success");
			})

	}
}

//method to fetch instant orders
const loadOrders = async ()=>{
	
	let isBranch = cookies.split(';').filter(cookie=>cookie.includes('bxnIsBranch'))[0].split('=')[1]
	let branches = []
	let orders = []
//	$.notify("Fetching instant orders...", "info");

	//fecth the business' branches if it has any
	if(isBranch == 'false'){
		try{
			branches = JSON.parse(cookies.split(';').filter(cookie=>cookie.includes('bxnBranchIds'))[0].split('=')[1])
		}catch(e){
			branches = []
		}
	}

	//fetch account instant orders
	db.collection('deliveries')
		.where('requestedBy', '==', uid)
		.where('status', 'in', ['pickup', 'pending', 'assigned', 'halfway', 'dropoff'])
		.where('requestTime', '<', end).where('requestTime', '>', start)
		.onSnapshot(snapshot=>{
			snapshot.forEach(doc=>{
				let data = doc.data()
                data._id = doc.id

                const elementsIndex = orders.findIndex(element => element._id === data._id )
                		
        		if(elementsIndex >= 0){

        			let newArray = [...orders]
        			newArray[elementsIndex] = {...newArray[elementsIndex], data}
        			orders = newArray

        		}else{
        			orders.push(data)
        		}

			})
			
			displayOrders(orders)
		})


	// fetch branches instant orders
	if(branches.length > 0){

		for(var branch of branches){

			db.collection('deliveries')
				.where('requestedBy', '==', branch)
				.where('status', 'in', ['pickup', 'pending', 'assigned', 'halfway', 'dropoff'])
				.where('requestTime', '<', end).where('requestTime', '>', start)
				.onSnapshot(snapshot=>{
					snapshot.forEach(doc=>{
						let data = doc.data()
		                data._id = doc.id

		                const elementsIndex = orders.findIndex(element => element._id == doc.id )
                		
                		if(elementsIndex >= 0){

                			let newArray = [...orders]
                			newArray[elementsIndex] = {...newArray[elementsIndex], data}
        					orders = newArray

                		}else{
                			orders.push(data)
                		}

					})
					
					displayOrders(orders)
				})
		}
	//	$.notify("Instant orders up to date!", "success");

	}else{

		const orders = []

			db.collection('deliveries')
			.where('requestedBy', '==', uid)
			.where('status', 'in', ['pickup', 'pending', 'assigned', 'halfway', 'dropoff'])
			.where('requestTime', '<', end).where('requestTime', '>', start)
			.onSnapshot(snapshot=>{
				snapshot.forEach(doc=>{
					let data = doc.data()
	                data._id = doc.id

	                const elementsIndex = orders.findIndex(element => element._id == doc.id )
                		
            		if(elementsIndex >= 0){

            			let newArray = [...orders]
            			newArray[elementsIndex] = {...newArray[elementsIndex], data}
        				orders = newArray

            		}else{
            			orders.push(data)
            		}

				})
				
				displayOrders(orders)
			//	$.notify("Instant orders up to date!", "success");
			})
	}
}

const displayOrders = (orders)=>{
	$('#orders-display').html('')

	let sortedOrders = orders.sort(function(a, b){ return b.requestTime - a.requestTime });

	sortedOrders = sortedOrders.filter((order, index, self) =>
	  index === self.findIndex((t) => {
	    return t._id === order._id// && (order.print == null || t.print)
	  })
	)

    $('#pagination').pagination({
        dataSource: sortedOrders,
        pageSize: 6,
        showGoInput: true,
        showGoButton: true,
        callback: function(data, pagination) {
            // template method of yourself
            var html = orderTemplate(data);
            $('#dataContainer').html(html);
        }
    })
}

const renderDeliveries = (deliveries, selection)=>{

	//handling pending
	$(`#${selection}-deliveries`).html('')

    $(`#${selection}-deliveries-pagination`).pagination({
        dataSource: deliveries,
        pageSize: 3,
        showNavigator: true,
        showPageNumbers: false,
        showGoInput: true,
        showGoButton: true,
        callback: function(data, pagination) {
            // template method of yourself
            var html = deliveryTemplate(data);
            $(`#${selection}-deliveries`).html(html);
        }
    })

}

$('#logout').on('click', e => {
	e.preventDefault()
	auth.signOut()
	var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    window.location.href = './'
})

$('#modal-cancel-accept').on('click', ()=>{
	let id = $('#cancel-id').val()

	db.collection("deliveries").doc(id).update("status", "cancelled").then(()=>{
		$.notify("You delivery request has been cancelled.", "info");
		page == 2 ?
		$('#deliveries').trigger('click') :
		$('#instant-orders').trigger('click')
	})
})