let firebaseConfig

// if(location.hostname === 'app.boxconn.co'){
	firebaseConfig = {
	    apiKey: "AIzaSyA7jH-X8CusG67x7Oc8OtK07EUKzxqZmLI",
	    authDomain: "boxconn.co",
	    databaseURL: "https://boxconn-technologies.firebaseio.com",
	    projectId: "boxconn-technologies",
	    storageBucket: "boxconn-technologies.appspot.com",
	    messagingSenderId: "1049831719040",
	    appId: "1:1049831719040:web:1d19232aa414d9f4932665",
	    measurementId: "G-P2BL4DT22J"
	};
// }else{
// 	firebaseConfig = {
// 	    apiKey: "AIzaSyBVx9536MPRnk-5Ecz2PS2mXudN2jkCUSw",
// 		authDomain: "boxconn-dev.firebaseapp.com",
// 		projectId: "boxconn-dev",
// 		storageBucket: "boxconn-dev.appspot.com",
// 		messagingSenderId: "844810412787",
// 		appId: "1:844810412787:web:c7b1ccae0d1c46e960cd86",
// 		measurementId: "G-3KJL2WGHNW"

// 	};
// }

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth()
const db = firebase.firestore()

export { auth, db }