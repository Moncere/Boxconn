import { db } from './config.js'

//single delivery order item
const deliveryTemplate = (data)=>{
	var html = ''

	for(var delivery of data){

		// html += `
		// 	<a data-toggle="modal" data-target="#cancel-modal" href="" data-id="${delivery.id}">
		// 		<div style="padding: 5px; margin-bottom: 5px;border: 1px solid #E9FAF7 !important; border-radius: 4px; background: #E9FAF7">
	 //                <p style='color: black'>Delivery from ${ delivery.data().pickup.name } to ${ delivery.data().dropoff.name }</p>
	 //            </div>
	 //        </a>
		// `

		html += `
				<tr  data-target="#myModal" data-toggle="modal" onclick="orderDetails('${delivery.id}', '${escape(JSON.stringify(delivery.data()))}')" style="cursor: pointer">
					<td class="single-delivery" scope="row">Delivery from ${ delivery.data().pickup.name } to ${ delivery.data().dropoff.name }</td>
				</tr>
			`
	}

	if(html === ''){
		html = `<div style="padding: 10px; margin-top: 5px">
                <p>No deliveries to display...</p>
            </div>`
	}

	return html
}

//single instant order item
const orderTemplate = (data)=>{

	var html = ''

	for(var order of data){

		let status = '', print = '', rider = '', cancel = ''

		//check status and show status pill accordingly
		if(order.status == 'completed'){
			status = '<img  src="images/completed.svg" width="120" alt="Order Completed" class="image">'
		}else if(order.status == 'pending'){
			status = '<img  src="images/pending_pill.svg" width="120" alt="Order Pending" class="image">'

			//check print status and render correct text
			if(order.print){
				print = 'Print Again <i class="fa fa-print"></i>'
			}else{
				print = 'Send Print Job <i class="fa fa-print"></i>'
			}
		}else if(order.status == 'assigned'){
			status = '<img  src="images/assigned.svg" width="120" alt="Order Assigned" class="image">'

			//check rider assign status and show details if assigned
			if(order.rider){
				rider = `
					<hr>
					<p style="color: #005376">${order.rider.name} [${order.rider.id}] will collect the order </p>
				`
			}
		}else if(order.status == 'cancelled'){
			status = '<img  src="images/cancelled.svg" width="120" alt="Order Cancelled" class="image">'
		}else{

			//display rider status
			if(order.rider){
				rider = `
					<hr>
					<p style="color: #005376">${order.rider.name} [${order.rider.id}] is on the way to deliver.</p>
				`
			}
		}

		let label = escape(`${ order.instructions }`)

		html += `
			<div class="white-box third">
				<div style="font-family: Ubuntu" class="box-padding">
		            <a href="" data-toggle="modal" data-target="#cancel-delivery" onclick="cancelDelivery('${order._id}', '${label}')" style="float:right">
		              <img src="images/cancel.svg" width="23" alt="">
		            </a>

		            <p style="font-size:10px">${order.pickup.name}</p>
		            ${status}
		            <h4 style="font-size:12px; height: 100px; display: block; overflow-y: scroll; overflow:auto;">${order.instructions}</h4>

		            <div>
		              <span style="font-size:12px">Price: GHS ${order.itemFee}</span>     
		              <span style="float:right; color: #005376" id="count-up-${order._id}"></span>  
		              <p v-if="kfc.includes($auth.user.email)">
		                <a href="#" onclick="print('${escape(JSON.stringify(order))}')" style="font-size: 11px;">${print}</a>
		              </p>      
		            </div>
		            
		            ${rider}

	          	</div>
	      	</div>`

	    // activate order specific timer
		timer(order)
	}

	if(html === ''){
		html = `
			<div class="white-box full-width" style="padding: 25px">
				<center><img src="images/empty.svg" width="350" alt="Empty Order State"></center>
			</div>`
	}

	return html
}

// timer function
const timer = (order)=>{
	setInterval(()=>{
	  try{
	      let t = Date.parse(new Date()) - Date.parse(order.requestTime.toDate());
	      let seconds = Math.floor((t / 1000) % 60);
	      let minutes = Math.floor((t / 1000 / 60) % 60);
	      let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
	      let days = Math.floor(t / (1000 * 60 * 60 * 24));
	      if (t > 0) {
	        let currentTime = {
	          total: t,
	          days: days,
	          hours: hours,
	          minutes: minutes,
	          seconds: seconds
	        };

	        if(currentTime.hours > 0){
	          try{
	            document.getElementById(`count-up-${order._id}`).innerHTML = hours+' hrs ' + minutes + ' mins'
	          }catch(e){}
	        }else{
	          try{
	            document.getElementById(`count-up-${order._id}`).innerHTML = minutes+' mins ' + seconds + ' secs';
	          }catch(e){ }
	        }

	        if(currentTime.hours > 0){
	            return hours+' hrs ' + minutes + ' mins'
	        }else{
	          return minutes+' mins ' + seconds + ' secs';
	        }
	      } else {
	        let currentTime = null;
	        return ''
	      }
	  }catch(e){}
	}, 1000)
}

export { deliveryTemplate, orderTemplate }